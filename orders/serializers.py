from rest_framework import serializers

from accounts.serializer import AccountSerializer
from .models import Order


class OrderSerializer(serializers.ModelSerializer):
    created_by = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Order
        fields = '__all__'

    def to_representation(self, instance):
        serialized_data = super(OrderSerializer, self).to_representation(instance)
        serialized_data["created_by"] = AccountSerializer(instance.created_by, context=self.context).data

        # if instance.is_paid:
        #     from transactions.utils import get_payment_method
        #
        #     serialized_data["payment_mode"] = get_payment_method(instance)
        return serialized_data
