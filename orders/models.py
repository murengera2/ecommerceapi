from django.db import models
from product.models import Product
from utils.functions import generate_code
from uuid import uuid4
from accounts.models import User


class Order(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid4)
    reference_code = models.CharField(max_length=30, default=generate_code)
    products = models.JSONField(default=list)
    subtotal = models.IntegerField()

    total = models.IntegerField()
    statuses = [
        ('NEW', 'NEW'),
        ('WAITING_PAYMENT_RESPONSE', 'WAITING_PAYMENT_RESPONSE'),
        ('PAYING', 'PAYING'),
        ('FINISHED', 'FINISHED'),
        ('ACTIVE', 'ACTIVE'),
        ('ON_WAY', 'ON_WAY'),
        ('DRIVER_ARRIVED', 'DRIVER_ARRIVED'),
        ('DENIED', 'DENIED'),
        ('CANCELLED', 'CANCELLED')
    ]
    status = models.CharField(max_length=40, choices=statuses, default='NEW')
    is_paid = models.BooleanField(default=False)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.reference_code
