import json

from django.shortcuts import render

from product.serializers import ProductSerializer
from .models import Order
from .serializers import OrderSerializer
from rest_framework.views import APIView
from rest_framework import status
from rest_framework import generics
from rest_framework import viewsets
# Create your views here.
from rest_framework.permissions import IsAdminUser, IsAuthenticated, AllowAny
from .permissions import IsOwner, IsOwnerOrAdmin
from product.models import Product, ProductCategory
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
import os


class OrderModel(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    pagination_class = None
    filterset_fields = ["created_by"]
    search_fields = ["created_by"]
    ordering_fields = [ "created_at", "created_by"]
    def get_permissions(self):
        if self.action in ['destroy', 'update', 'retrieve']:
            self.permission_classes = [IsOwnerOrAdmin]
        elif self.action in ['create', 'list']:
            self.permission_classes = [IsAuthenticated]
        return super(OrderModel, self).get_permissions()


def create(self, request, *args, **kwargs):
    global product_data
    products_ids = request.data
    total = 0
    products = []
    context = {
        "request": request
    }
    for pid in products_ids:
        product = Product.objects.filter(id=pid.get("id"), is_deleted=False).first()
        product_total = product.price
        if not product:
            return Response({"detail": "One or more products are not available in stock"}, status=400)
        quantity = pid.get("quantity", 0)

        product_data = ProductSerializer(product, context=context).data
        product_data["quantity"] = quantity
        product_data["total"] = product_total * quantity

        total = total + (product_total * quantity)

        print(total)
        print(product_data)
        products.append(json.loads(JSONRenderer().render(product_data)))

    charge = os.getenv("CHARGE", 0)

    fees = int(total * charge)

    data = {
        "products": product_data,
        "subtotal": total,
        "fees": fees,
        "total": fees + total,
        "created_by": request.user
    }
    print((data))
    serializer = OrderSerializer(data=data, context=context)

    serializer.is_valid(raise_exception=True)
    serializer.save()
    return Response(serializer.data, status=200)
