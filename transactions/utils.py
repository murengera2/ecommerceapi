from orders.models import Order
from .rave import make_payment_request
from transactions.models import Transaction
from accounts.models import User


def check_order(order, user):
    ALLOWED_STATUSES = [
        "PAYING"
    ]

    if order.is_paid:
        return {"detail": "Order has been already paid"}, 400

    """
    if order.status not in ALLOWED_STATUSES:
        return {"detail": "Order with {status} cannot be paid".format(status=order.status)}, 400
    """

    transactions = Transaction.objects.filter(order=order, status="WAITING") | Transaction.objects.filter(order=order,
                                                                                                          status="SUCCESSFUL")

    if len(transactions) > 0:
        transaction = transactions[0]
        if transaction.payment_url:
            data = {
                "pay_link": transaction.payment_url
            }
            return data, 200
        return {"detail": "There is a pending transaction already"}, 409

    Transaction.objects.filter(order=order, status="UNPROCESSED").update(status="FAILED")

    transaction = Transaction(
        order=order,
        created_by=user,
        status="UNPROCESSED",
        total_amount=order.total

    )

    transaction.save()

    payload = {
        "tx_ref": str(transaction.id),
        "customer": {

            "email": user.email,
            "phonenumber": user.phone_number,
            "name": user.get_full_name()
        },
        "amount": int(transaction.total_amount),
        "customizations": {
            "logo": "https://www.emall.rw/wp-content/uploads/2020/10/snip-logo.png",
            "title": "Pay for your order {}".format(order.reference_code),
            "description": "Thank you for using JShine solutions! You are about to finish with all steps! Select your "
                           "preferred payment method, and click pay, to finish."
        },

    }

    return make_payment_request(payload, transaction)


def get_payment_method(order):
    transaction = Transaction.objects.filter(order=order, status="SUCCESSFUL").first()

    print("***********transaction*************",transaction)

    if transaction:
        return transaction.payment_mode
    return None
