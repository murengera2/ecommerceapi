from datetime import timedelta

import requests
from django.utils import timezone

from transactions.models import Transaction
from decouple import config

PAYMENT_URL = config("RAVE_PAYMENT_REQUEST_URL")
VERIFY_URL = config("RAVE_PAYMENT_VERIFY_URL")
PBFPubKey = config("RAVE_PBFPUBKEY")
SECKEY = config("RAVE_SECKEY")
CALLBACK_URL = config('RAVE_CALLBACK_URL')

HEADERS = {
    "Accept": "application/json",
    "Content-Type": "application/json",
    "Authorization": f"Bearer {SECKEY}"
}


def make_payment_request(payload, transaction):
    payload["currency"] = "RWF"
    payload["country"] = "RW"
    payload["email"] = "daltonbigirimana5@gmail.com"
    payload["payment_options"] = "mobilemoneyrwanda"
    payload["phone_number"] = "0784870429"
    payload["PBFPubKey"] = PBFPubKey
    payload["redirect_url"] = CALLBACK_URL

    transaction.payment_request_content = payload
    transaction.payment_response_content = payload
    print("transaction", transaction)
    transaction.save()
    print("******payload********", payload)

    response = requests.post(PAYMENT_URL, json=payload, headers=HEADERS)
    print("**************reponse**********", response)

    # INITIATING SUCCESSFUL
    rj = response.json()

    print("************rj**********", rj)
    # print("**********payload**********",payload)
    if response.status_code == 200:
        transaction.status = "WAITING"
        transaction.payment_url = rj.get("meta", {}).get("authorization", {}).get("redirect")
        transaction.save()
        data = {
            "pay_link": rj.get("meta", {}).get("authorization", {}).get("redirect")
        }
        print("********data******", data)
        return data, 200
    else:
        transaction.status = "FAILED"
        try:
            message = rj["message"]
            return {"detail": message}, 400
        except Exception as e:
            return {"detail": str(e)}, 400


def verify_payment(transaction_id, status, tx_id):
    transaction = Transaction.objects.filter(id=transaction_id).first()

    if not transaction:
        return False

    url = f"{VERIFY_URL}{tx_id}/verify"

    response = requests.get(url, headers=HEADERS)

    rj = response.json()

    status = rj.get("status")

    if status == "success":
        transaction.status = "SUCCESSFUL"
        transaction.payment_mode = rj.get("data").get("payment_type")

        order = transaction.order
        order.is_paid = True
        order.save()

        message = f"Dear {order.created_by.get_full_name}, we have received your payment for order #{order.reference_code}."
        phone_number = order.created_by.phone_number

        transaction.payment_response_content = rj
        transaction.save()
        # send_sms_task(message=message, phone_numbers=[phone_number])

    else:
        transaction.status = "FAILED"

    url = f"http://localhost:4200/#/order/{transaction.order.id}"
    return True, url
