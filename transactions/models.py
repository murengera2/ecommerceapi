from django.db import models

# Create your models here.
from django.db import models
from orders.models import Order
from uuid import uuid4
from accounts.models import User
from utils.functions import generate_code


class Transaction(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid4)
    reference_code = models.CharField(max_length=100, default=generate_code)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    payment_mode = models.CharField(max_length=300, null=True, blank=True)

    statuses = [
        ('UNPROCESSED', 'UNPROCESSED'),
        ('WAITING', 'WAITING'),
        ('SUCCESSFUL', 'SUCCESSFUL'),
        ('FAILED', 'FAILED')
    ]

    status = models.CharField(max_length=50, choices=statuses, default='UNPROCESSED')
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
    payment_response_content = models.JSONField(null=True, blank=True)
    payment_request_content = models.JSONField(null=True, blank=True)
    payment_url = models.URLField(null=True, blank=True)
    total_amount = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.reference_code
