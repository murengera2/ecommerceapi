from django.shortcuts import render
# Create your views here.
from django.shortcuts import redirect
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated, AllowAny
from orders.models import Order
from rest_framework.response import Response
from .models import Transaction
from .serializers import TransactionSerializer
from .utils import check_order
from .rave import verify_payment


class TransactionViewsets(viewsets.ReadOnlyModelViewSet):
    serializer_class = TransactionSerializer
    permission_classes = [IsAuthenticated]
    queryset = Transaction.objects.none()
    filter_fields = ["id", "reference_code", "order", "status", "payment_mode"]
    ordering_fields = ["created_at", "reference_code", "order", "status", "payment_mode"]
    ordering = "-created_at"

    def get_queryset(self):
        if self.request.user.is_staff:
            return Transaction.objects.all()
        return Transaction.objects.filter(created_by=self.request.user)


class TransactionActionsViewsets(viewsets.ViewSet):
    permission_classes = [IsAuthenticated]

    @action(detail=False, methods=["post"], url_path="make-payment", name="make-payment")
    @swagger_auto_schema(
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'order': openapi.Schema(type=openapi.TYPE_STRING, description='Order id'),
            }
        ),
        responses={
            status.HTTP_200_OK: openapi.Response(
                description="Payment link is generated",
                examples={
                    "application/json": {
                        "pay_link": "https://rave.flutterwave.com/pay"
                    }
                }
            ),
            status.HTTP_400_BAD_REQUEST: openapi.Response(
                description="Several failures which can cause bad request",
                examples={
                    "application/json": {
                        "detail": "Ticket is not found | Ticket ABC123 has been already paid | Transaction error"
                    },
                }
            ),
        })
    def make_payment(self, request):

        if request.user.is_anonymous:
            return Response({"detail": "Login credentials were not supplied"}, status=401)

        order = Order.objects.filter(id=request.data.get("order"), created_by=request.user).first()
        print("+++++++++++++++order++++++++++++++",order)

        if not order:
            return Response({"detail": "Order is not found"}, status=400)

        response, status = check_order(order=order, user=request.user)
        print("___________response___________",response,status)

        return Response(response, status=status)

    @action(detail=False, methods=["get", "post"], url_path="verify-payment", name="verify-payment", url_name="verify-payment")
    @swagger_auto_schema(
        manual_parameters=[openapi.Parameter(
            name="txref",
            in_="txref",
            required=True,
            type=openapi.TYPE_STRING,
            description="txref for the transaction ID to verify",

        )],
        responses={
            status.HTTP_200_OK: openapi.Response(
                description="Redirect to relevant page (success or failed)",
            )
        })
    def payment_callback(self, request):
        is_paid, url = verify_payment(request.query_params.get('tx_ref'), request.query_params.get('status'), request.query_params.get('transaction_id'))
        print("**********url*********",url)
        if is_paid:
            return redirect(url)
        return redirect(url)
