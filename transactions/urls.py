from .views import TransactionViewsets, TransactionActionsViewsets
from django.urls import path, include
from rest_framework.routers import DefaultRouter

routes = DefaultRouter(trailing_slash=False)
routes.register("transactions", TransactionViewsets)
routes.register("transaction-actions", TransactionActionsViewsets, basename="transaction-actions")

urlpatterns = [
    path("", include(routes.urls))
]
