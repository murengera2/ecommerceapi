from rest_framework import serializers

from accounts.serializer import AccountSerializer
from .models import Transaction


class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = '__all__'

    def to_representation(self, instance):
        serialized_data = super(TransactionSerializer, self).to_representation(instance)
        serialized_data["created_by"] = AccountSerializer(instance.created_by, context=self.context).data
        return serialized_data
