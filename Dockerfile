FROM python:3.6.10-alpine3.11

# set work directory
RUN mkdir /app
WORKDIR /app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1


# install psycopg2 dependencies for Postgresql
# install dependencies
RUN pip install --upgrade pip
COPY ./requirements.txt requirements.txt
RUN pip install -r requirements.txt

# copy project
COPY . /app
# COPY . /usr/src/app

# create and run user
RUN adduser -D dalton
USER dalton

