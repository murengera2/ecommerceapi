from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from django.contrib import messages
from rest_framework.decorators import api_view, action, permission_classes
from rest_framework.response import Response
from .models import User
from accounts.serializer import AccountSerializer
from rest_framework.views import APIView
from rest_framework import status
from rest_framework import generics
from rest_framework import viewsets
from .permissions import IsOwner
from rest_framework.permissions import IsAdminUser, AllowAny, IsAuthenticated
from rest_framework.authtoken.models import Token
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.password_validation import (
    UserAttributeSimilarityValidator,
    CommonPasswordValidator,
    MinimumLengthValidator,
    NumericPasswordValidator,
)

from django.core.exceptions import ValidationError


class UserModel(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = AccountSerializer

    def get_permissions(self):

        if self.action in ('update', 'retrieve', 'destroy'):
            self.permission_classes = [IsOwner]

        elif self.action == 'list':
            self.permission_classes = [IsAdminUser]

        else:
            self.permission_classes = [AllowAny]

        return super(self.__class__, self).get_permissions()


class AuthenticationActions(viewsets.ViewSet):
    permission_classes = [AllowAny]

    @action(detail=False, methods=['post'], url_path="login", name='login')
    @swagger_auto_schema(
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'username': openapi.Schema(type=openapi.TYPE_STRING, description='Email or phone number'),
                'password': openapi.Schema(type=openapi.TYPE_STRING, description='Password'),
            },
            required=['username', 'password']
        ),
        responses={
            status.HTTP_200_OK: openapi.Response(
                description="Verification code has been verified",
                examples={
                    "application/json": {
                        "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
                        "first_name": "string",
                        "last_name": "string",
                        "email": "user@example.com",
                        "phone_number": "string",
                        "is_active": True,
                        "is_staff": True,
                        "token": "lfa85f64-5717-4562-b3fc-2c963f66afa9"
                    }
                }
            ),
            status.HTTP_400_BAD_REQUEST: openapi.Response(
                description="Login errors: Verification code is expired, or invalid credentials",
                examples={
                    "application/json": {
                        "detail": "Session has expired | Invalid credentials"
                    },
                }
            ),
        })
    def signin(self, request, *args, **kwargs):
        username = request.data.get('username')
        password = request.data.get('password')

        user = authenticate(request=request, username=username, password=password)

        if not user:
            return Response({'detail': 'Invalid credentials'}, status=400)
        login(request=request, user=user)
        context = {
            'request': request
        }
        data = AccountSerializer(user, context=context).data
        token, _ = Token.objects.get_or_create(
            user=user
        )
        data['token'] = token.key

        return Response(data, status=200)

    @action(detail=False, methods=['get'], url_path="logout", name='logout')
    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: openapi.Response(
                description="Successfully signed out",
                examples={
                    "application/json": {
                        "detail": "Signed out"
                    }
                }
            )
        })
    def signout(self, request):
        if request.user.is_anonymous:
            return Response({"detail": "You are not allowed to perform this operation"}, status=403)
        Token.objects.filter(user=request.user).delete()
        logout(request)
        return Response({"detail": "Signed out"}, status=200)

    @action(detail=False, methods=['post'], url_path="create-account", name='create-account')
    @swagger_auto_schema(
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'email': openapi.Schema(type=openapi.TYPE_STRING, description='Email'),
                'phone_number': openapi.Schema(type=openapi.TYPE_STRING, description='Phone number'),
                'first_name': openapi.Schema(type=openapi.TYPE_STRING, description='First name'),
                'last_name': openapi.Schema(type=openapi.TYPE_STRING, description='Last name'),
                'password': openapi.Schema(type=openapi.TYPE_STRING, description='Password'),
            },
            required=['email', 'phone_number', 'first_name', 'last_name', 'password']
        ),
        responses={
            status.HTTP_200_OK: openapi.Response(
                description="Account created",
                examples={
                    "application/json": {
                        "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
                        "first_name": "string",
                        "last_name": "string",
                        "email": "user@example.com",
                        "phone_number": "string",
                        "is_active": True,
                        "is_staff": True,
                        "token": "lfa85f64-5717-4562-b3fc-2c963f66afa9"
                    }
                }
            ),
            status.HTTP_400_BAD_REQUEST: openapi.Response(
                description="Account/verification code exception",
                examples={
                    "application/json": {
                        "detail": "Bad request"
                    },
                }
            ),
            status.HTTP_409_CONFLICT: openapi.Response(
                description="Account already exists",
                examples={
                    "application/json": {
                        "detail": "Account with this email/phone number already exists"
                    },
                }
            )
        })
    def create_account(self, request):
        context = {
            "request": request
        }
        sent_password = request.data.get("password")

        # password_validators = [
        #     UserAttributeSimilarityValidator,
        #     CommonPasswordValidator,
        #     MinimumLengthValidator,
        #     NumericPasswordValidator
        # ]

        # try:
        #     for validator in password_validators:
        #         validator().validate(sent_password)
        # except ValidationError as e:
        #     messages.error(request, str(e))
        #     return Response({"detail": e}, status=400)

        usr = User.objects.filter(email=request.data.get("email")).first()
        if usr:
            return Response({"detail": "Account with this email already exists"}, status=409)

        usr = User.objects.filter(phone_number=request.data.get("phone_number")).first()

        if usr:
            return Response({"detail": "Account with this phone number already exists"}, status=409)

        serializer = AccountSerializer(data=request.data, context=context)
        serializer.is_valid(raise_exception=True)

        user = serializer.save()
        print("sent password",sent_password)


        user.set_password(sent_password)
        print("user",user)
        user.save()

        token, _ = Token.objects.get_or_create(
            user=user
        )
        data = serializer.data
        data['token'] = token.key
        print("data",data)
        return Response({"detail": data},
                        status=201)


