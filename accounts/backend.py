"""
Backend that allows authentication using national_id and phone_number
"""

from accounts.models import User
from django.contrib.auth.backends import ModelBackend


class Backend(ModelBackend):
    def authenticate(self, request=None, **kwargs):
        """
                Returns a user with the given credentials
                :param request: Request object passed to the backend
                :param kwargs: Contains credentials to find user
                :return: User if credentials are found
                """

        username = kwargs.get('username')  # contains phone_number or national_id
        password = kwargs.get('password')
        # try phone number
        user = User.objects.filter(phone_number=username).first()

        # try national id
        if not user:
            user = User.objects.filter(national_id=username).first()
        if user:
            if user.check_password(password) and user.is_active:
                return user
        return None

    def get_user(self, user_id):
        """
                Returns instance of the user, when given the user_id
                :param user_id: User id, the current primary key
                :return: User if found and None otherwise
                """
        try:
            return User.objects.get(pk=user_id)

        except User.DoesNotExist:
            return None
