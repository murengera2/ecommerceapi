from django.urls import path, include
from rest_framework.routers import DefaultRouter
from . import views

routes = DefaultRouter(trailing_slash=False)
routes.register('UserModel', views.UserModel)
routes.register('authentication', views.AuthenticationActions, basename='authentication')



urlpatterns = [
    path('', include(routes.urls)),
  
]
