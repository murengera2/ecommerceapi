from rest_framework.permissions import BasePermission
from .models import User


class IsOwner(BasePermission):

    def has_object_permission(self, request, view, obj):

        if request.user.is_superuser:
            return True

        if request.user.is_authenticated:

            return request.user == obj
        else:
            return False
