from django.contrib.auth.models import AbstractUser
from django.db import models
import uuid

from accounts.manager import UserManager

"""
Backend that allows authentication using national_id or phone_number
"""


#   Custom user
class User(AbstractUser):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    name = models.CharField(max_length=255,blank=True,default='dalton')
    username = models.CharField(max_length=255, blank=True)
    phone_number = models.CharField(max_length=15, unique=True,blank=True,null=True)
    national_id = models.CharField(max_length=20, unique=True,blank=True,null=True)
    first_name = models.CharField(max_length=300, blank=True,)
    last_name = models.CharField(max_length=300, blank=True)
    email = models.EmailField(max_length=255, unique=True, null=True, blank=True)
    is_deleted = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    USERNAME_FIELD = 'phone_number'
    REQUIRED_FIELDS = ['email', 'national_id', 'name', 'first_name', 'last_name']
    objects = UserManager()

    def save(self, *args, **kwargs):
        if not str(self.phone_number).startswith('+'):
            raise Exception('phone number must start with a +')
        # if len(str(self.national_id)) < 16:
        #     raise Exception('national id must be greater than 16')

        super(User, self).save(*args, **kwargs)

    def __str__(self):
        return self.phone_number
