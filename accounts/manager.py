"""
custom user manager for roots origin project
"""
from django.contrib.auth.models import  UserManager as UManager
class UserManager(UManager):
    """
      Customises Django User Manager, by replacing the required username with national_id,phone_number
      Creating users require the national_id or phone_number not username.
      """

    def _create_user(self, phone_number, national_id, password, **extra_fields):

        if not phone_number:
            raise  ValueError('the given phone number must be set')
        if not national_id:
            raise  ValueError('the given national_id must be set')

        user=self.model(phone_number=phone_number,national_id=national_id, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return  user
    def create_user(self, phone_number, national_id=None, password=None, **extra_fields):
        extra_fields.setdefault('is_staff',False)
        extra_fields.setdefault('is_superuser',False)
        return  self._create_user(phone_number=phone_number,national_id=national_id,password=password)

    def create_superuser(self, phone_number=None, password=None, national_id=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(phone_number=phone_number,national_id=national_id,password=password,**extra_fields)


