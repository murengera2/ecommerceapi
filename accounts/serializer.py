from rest_framework import serializers
from .models import User


class AccountSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = ["id", "name","last_name","first_name", "email", "phone_number","national_id","email","is_deleted","is_active","username"]


