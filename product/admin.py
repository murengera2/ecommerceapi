from django.contrib import admin
from .models import Product, ProductCategory,ProductBrand

admin.site.register(Product)
admin.site.register(ProductCategory)
admin.site.register(ProductBrand)
