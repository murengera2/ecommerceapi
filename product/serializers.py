from rest_framework import serializers
from product.models import Product, ProductCategory, ProductBrand


class ProductCategorySerializer(serializers.ModelSerializer):
    created_by = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = ProductCategory
        fields = '__all__'


class ProductbrandSerializer(serializers.ModelSerializer):
    created_by = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = ProductBrand
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):
    created_by = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Product
        fields = '__all__'

    def to_representation(self, instance):
        serialized_data = super(ProductSerializer, self).to_representation(instance)
        serialized_data["category"] = ProductCategorySerializer(instance.category).data
        serialized_data["brand"] = ProductbrandSerializer(instance.brand).data
        return serialized_data
