from django.urls import path, include
from rest_framework.routers import DefaultRouter
from  product import views

routes = DefaultRouter(trailing_slash=False)
routes.register('ProductModel', views.ProductModel)
routes.register('category', views.ProductCategoryModel)
routes.register('brand', views.ProductBrandModel)

urlpatterns = [
    path('', include(routes.urls)),
]
