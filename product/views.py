
from django.shortcuts import render
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.decorators import api_view
from rest_framework.pagination import PageNumberPagination

from ecommerceapi import settings
# Create your views here.
from .models import Product, ProductCategory, ProductBrand
from .serializers import ProductSerializer, ProductCategorySerializer, ProductbrandSerializer
from rest_framework.views import APIView
from rest_framework import status
from rest_framework import generics
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.permissions import IsAdminUser, IsAuthenticated


class ProductModel(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


    filterset_fields = ["id", "category", "name","brand"]
    search_fields = ["name", "description", "category__name", "brand__name"]
    ordering_fields = ["name", "created_at", "price"]


    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()

        instance.is_deleted = True
        instance.save()
        return Response({'detail': 'Product deleted'}, status=204)

    def get_permissions(self):

        if self.action in ['destroy', 'update', 'create']:
            self.permission_classes = [IsAdminUser]

        if self.action == 'retrieve':
            self.permission_classes = []

        return super(self.__class__, self).get_permissions()


class ProductCategoryModel(viewsets.ModelViewSet):
    queryset = ProductCategory.objects.all()
    serializer_class = ProductCategorySerializer

    filterset_fields = ["id"]
    search_fields = ["name", ]
    ordering_fields = ["created_at", "name"]

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.is_deleted = True
        instance.save()
        return Response({'detail': 'Product category deleted'}, status=204)

    def get_permissions(self):

        if self.action in ['destroy', 'update', 'create']:
            self.permission_classes = [IsAdminUser]

        if self.action == 'retrieve':
            self.permission_classes = [IsAuthenticated]

        return super(self.__class__, self).get_permissions()


class ProductBrandModel(viewsets.ModelViewSet):
    queryset = ProductBrand.objects.all()
    serializer_class = ProductbrandSerializer


    filter_fields = ["id", "name"]
    search_fields = ["name", ]
    ordering_fields = ["created_at", "name"]

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()

        instance.is_deleted = True
        instance.save()
        return Response({'detail': 'Product Brand deleted'}, status=204)

    def get_permissions(self):

        if self.action in ['destroy', 'update', 'create']:
            self.permission_classes = [IsAdminUser]

        if self.action == 'retrieve':
            self.permission_classes = [IsAuthenticated]

        return super(self.__class__, self).get_permissions()

